package io.github.minixz.animatedzoom.gui;

import io.github.minixz.animatedzoom.config.Config;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.SliderPercentageOption;
import net.minecraft.util.text.StringTextComponent;

public class ConfigGui extends Screen {
    private int zoomSpeed;

    public ConfigGui() {
        super(new StringTextComponent("Animated Zoom"));
    }

    @Override
    protected void init() {
        zoomSpeed = Config.zoomSpeed;

        addButton(
            new SliderPercentageOption(
                "settings.zoomSpeed.label",
                0.5,
                10.0,
                0.5f,
                (gameSettings) -> zoomSpeed / 1000.0,
                (gameSettings, value) -> {
                    zoomSpeed = (int) (value * 1000.0);
                },
                (gameSettings, option) -> {
                    return String.format(
                        "%s %.1f %s",
                        option.getDisplayString(),
                        zoomSpeed / 1000.0,
                        I18n.format("settings.zoomSpeed.suffix")
                    );
                }
            ).createWidget(
                minecraft.gameSettings,
                width / 2 - 150 / 2,
                height / 2 - 20 / 2,
                150
            )
        );

        addButton(
            new Button(
                width / 2 - 100 / 2,
                height / 2 - 25 / 2 + 30,
                100,
                20,
                I18n.format("gui.done"),
                this::handleSave
            )
        );
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        renderBackground();
        drawCenteredString(font, title.getFormattedText(), width / 2, 8, 16777215);
        super.render(mouseX, mouseY, partialTicks);
    }

    private void handleSave(Button ev) {
        Config.CLIENT_MOD_CONFIG.getConfigData().set("zoomSpeed", zoomSpeed);
        Config.CLIENT_MOD_CONFIG.save();
        getMinecraft().displayGuiScreen(null);
    }

    public static void open() {
        Minecraft.getInstance().displayGuiScreen(new ConfigGui());
    }
}
