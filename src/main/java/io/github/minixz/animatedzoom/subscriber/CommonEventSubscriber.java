package io.github.minixz.animatedzoom.subscriber;

import io.github.minixz.animatedzoom.Main;
import io.github.minixz.animatedzoom.gui.ConfigGui;
import net.minecraft.client.Minecraft;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.io.IOException;

@Mod.EventBusSubscriber(modid = Main.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class CommonEventSubscriber {
    private Main mod;
    private Minecraft minecraft;

    public CommonEventSubscriber(Main mod) {
        this.mod = mod;
        this.minecraft = Minecraft.getInstance();
    }

    @SubscribeEvent
    public void tick(TickEvent.ClientTickEvent ev) {
        if (ev.phase != TickEvent.Phase.END) {
            return;
        }

        if (minecraft.currentScreen != null || minecraft.player == null) {
            return;
        }

        if (mod.keyBindings.openConfig.isKeyDown()) {
            ConfigGui.open();
        }
    }
}
