package io.github.minixz.animatedzoom.subscriber;

import io.github.minixz.animatedzoom.Constants;
import io.github.minixz.animatedzoom.Main;
import io.github.minixz.animatedzoom.config.Config;
import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.EntityViewRenderEvent;
import net.minecraftforge.client.event.FOVUpdateEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.io.IOException;

@Mod.EventBusSubscriber(modid = Main.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ZoomEventSubscriber {
    private Main mod;
    private Minecraft minecraft;

    private boolean isZoomingIn = false;
    private double startZoomingTime = 0.0;
    private double startZoomingFov = 0.0;
    private double endZoomingFov = 0.0;
    private double currentFov = 0.0;

    public ZoomEventSubscriber(Main mod) {
        this.mod = mod;
        this.minecraft = Minecraft.getInstance();
    }

    @SubscribeEvent
    public void tick(TickEvent.ClientTickEvent ev) {
        if (ev.phase != TickEvent.Phase.START) {
            return;
        }

        if (minecraft.currentScreen != null || minecraft.player == null) {
            return;
        }

        if (mod.keyBindings.zoomIn.isKeyDown()) {
            if (!isZoomingIn) {
                isZoomingIn = true;
                startZoomingTime = System.currentTimeMillis();
            }
        } else if (isZoomingIn) {
            startZoomingTime = System.currentTimeMillis();
            endZoomingFov = currentFov;
            isZoomingIn = false;
        }
    }

    @SubscribeEvent
    public void onFOVModifier(EntityViewRenderEvent.FOVModifier ev) {
        if (isZoomingIn) {
            startZoomingFov = ev.getFOV();
            double time = System.currentTimeMillis() - startZoomingTime;
            double value = quadraticEasingIn(time, 0, startZoomingFov, Config.zoomSpeed);
            currentFov = Math.max(Constants.MIN_FOV, startZoomingFov - value);
            ev.setFOV(currentFov);
        } else if (currentFov < startZoomingFov) {
            startZoomingFov = ev.getFOV();
            double time = System.currentTimeMillis() - startZoomingTime;
            double value = quadraticEasingIn(time, endZoomingFov, startZoomingFov, Config.zoomSpeed);
            currentFov = Math.min(value, startZoomingFov);
            ev.setFOV(value);
        }
    }

    private double quadraticEasingIn(double currentTime, double startValue, double changeInValue, double duration) {
        return changeInValue * (currentTime /= duration) * currentTime + startValue;
    }
}
