package io.github.minixz.animatedzoom.config;

import io.github.minixz.animatedzoom.Main;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import org.apache.commons.lang3.tuple.Pair;

@Mod.EventBusSubscriber(modid = Main.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class Config {
    public static final ClientConfig CLIENT;
    public static final ForgeConfigSpec CLIENT_SPEC;

    public static ModConfig CLIENT_MOD_CONFIG;

    static {
        final Pair<ClientConfig, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(ClientConfig::new);
        CLIENT_SPEC = specPair.getRight();
        CLIENT = specPair.getLeft();
    }

    public static int zoomSpeed;

    @SubscribeEvent
    public static void onModConfigEvent(final ModConfig.ModConfigEvent ev) {
        CLIENT_MOD_CONFIG = ev.getConfig();
        bakeConfig();
    }

    public static void bakeConfig() {
        zoomSpeed = CLIENT.zoomSpeed.get();
    }
}
