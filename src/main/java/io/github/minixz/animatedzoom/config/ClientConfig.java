package io.github.minixz.animatedzoom.config;

import io.github.minixz.animatedzoom.Main;
import net.minecraftforge.common.ForgeConfigSpec;

public class ClientConfig {
    public ForgeConfigSpec.IntValue zoomSpeed;

    public ClientConfig(ForgeConfigSpec.Builder builder) {
        zoomSpeed = builder
                .comment("Zoom speed in ms")
                .translation(Main.MODID + ".config.zoomSpeed")
                .defineInRange("zoomSpeed", 1000, 0, 10000);
    }
}
