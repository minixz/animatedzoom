package io.github.minixz.animatedzoom;

import io.github.minixz.animatedzoom.config.Config;
import io.github.minixz.animatedzoom.subscriber.CommonEventSubscriber;
import io.github.minixz.animatedzoom.subscriber.ZoomEventSubscriber;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(Main.MODID)
public class Main {
    public static final String MODID = "animatedzoom";
    public static final Logger LOGGER = LogManager.getLogger(MODID);

    public final KeyBindings keyBindings;

    public Main() {
        ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, Config.CLIENT_SPEC);

        keyBindings = new KeyBindings();
        keyBindings.register();

        MinecraftForge.EVENT_BUS.register(new ZoomEventSubscriber(this));
        MinecraftForge.EVENT_BUS.register(new CommonEventSubscriber(this));
    }
}
