package io.github.minixz.animatedzoom;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import org.lwjgl.glfw.GLFW;

public class KeyBindings {
    public final String category = "Animated Zoom";
    public final KeyBinding zoomIn;
    public final KeyBinding openConfig;

    public KeyBindings() {
        zoomIn = new KeyBinding("Zoom In", GLFW.GLFW_KEY_C, category);
        openConfig = new KeyBinding("Open Config", GLFW.GLFW_KEY_Z, category);
    }

    public void register() {
        ClientRegistry.registerKeyBinding(zoomIn);
        ClientRegistry.registerKeyBinding(openConfig);
    }
}
